import { Component, OnInit, Inject } from '@angular/core';

import { Dish } from '../shared/dish';
import { DishService } from '../services/dish.service';

import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import 'rxjs/add/operator/switchMap';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Comment } from '../shared/comment';

import { visibility, flyInOut, expand } from '../animations/app.animation';

//import { Slider } from '../shared/slider';


@Component({
  selector: 'dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
    },
  animations: [
    visibility(),
    flyInOut(),
    expand()
  ]
})
export class DishdetailComponent implements OnInit {

  visibility = 'shown';
  dish: Dish;
  dishcopy = null;
  dishIds: number[];
  prev: number;
  next: number;
  errMess: string;
  commentForm: FormGroup;
  comment: Comment;
  commentFormErrors = {
    'author': '',
    'comment': ''
  };
  commentFormValidationMessages = {
    'author': {
      'required': 'Name is required.',
      'minlength': 'Name must contain atleast 2 characters.',
      
    },
    'comment': {
      'required': 'Comment is required.'
    },

  };

  constructor(private dishservice: DishService, 
    private route: ActivatedRoute, 
    private location: Location, 
    private fb: FormBuilder,
    @Inject('BaseURL') private BaseURL) {
      this.createForm1();
   }

  ngOnInit() {
    let id = +this.route.snapshot.params['id'];
    this.dishservice.getDish(id).subscribe(dish => this.dish = dish);
    this.dishservice.getDishIds().subscribe(dishIds => this.dishIds = dishIds);
    this.route.params
      .switchMap((params: Params) => { this.visibility = 'hidden'; return this.dishservice.getDish(+params['id']); })
      .subscribe(dish => { this.dish = dish; this.dishcopy = dish; this.setPrevNext(dish.id); this.visibility = 'shown'; }, 
        errmess => {this.dish = null; this.errMess = <any>errmess;} );
  }
  

  goBack(): void {
    this.location.back();
  }

  setPrevNext(dishId: number) {
    let index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1)%this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1)%this.dishIds.length];
  }

  createForm1(): void {
    this.commentForm = this.fb.group({
      rating: ['5'],
      author: ['', Validators.required, Validators.minLength(2)],
      comment: ['', Validators.required],
      date: ['']
    });
    this.commentForm.valueChanges.subscribe(data1 => this.commentFormonValueChanged(data1));
    this.commentFormonValueChanged();
  }

  onSubmit(){
    var d = new Date();
    this.comment = this.commentForm.value;
    this.comment.date = d.toISOString();
    this.dishcopy.comments.push(this.comment);
    this.dishcopy.save().subscribe( dish => { this.dish = dish; console.log(this.dish); });
    console.log(this.comment);
    this.commentForm.reset({
      rating: '5',
      comment: '',
      author: '',
      date: ''
    });
  }

  commentFormonValueChanged(data1?: any) {
    if (!this.commentForm) { return; }
    const form1 = this.commentForm;
    for (const field1 in this.commentFormErrors) {
      this.commentFormErrors[field1] = '';
      const control1 = form1.get(field1);
      if (control1 && control1.dirty && !control1.valid) {
        const messages1 = this.commentFormValidationMessages[field1];
        for (const key1 in control1.errors) {
          this.commentFormErrors[field1] += messages1[key1] + ' ';
        }
      }
    }
  }

}
